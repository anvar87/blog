@extends('layout')
@section('content')
    <!------  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"
          id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    Include the above in your HEAD tag ---------->
    <div class="container">
        <div class="row">

            <div class="col-lg-12">
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif

                <h2 class="login-box-msg">Регистрация
                </h2>
                <form action="{{ url('register') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label for="exampleInputName">Name</label><span class="text-danger" title="This field is required">*</span>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" aria-describedby="nameHelp" placeholder="Enter name">
                       @if ($errors->has('name'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label><span class="text-danger" title="This field is required">*</span>
                        <input type="email" class="form-control" name="email" value="{{old('email')}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        @if ($errors->has('email'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label><span class="text-danger" title="This field is required">*</span>
                        <input type="password" class="form-control" name='password' id="exampleInputPassword1" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Confirm password</label><span class="text-danger" title="This field is required">*</span>
                        <input type="password" class="form-control" name='password_confirmation' placeholder="Confirm password">
                    </div>
                    {{--<div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>--}}
                    <button type="submit" class="btn btn-primary">Register</button>
                </form>
            </div>
        </div>
    </div>
@endsection