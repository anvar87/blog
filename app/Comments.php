<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    use Notifiable;
    protected $table = 'comments';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_post', 'id_user', 'comment',
    ];


}
