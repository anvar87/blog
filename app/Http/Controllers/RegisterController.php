<?php

namespace App\Http\Controllers;

use App\User;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    public function index()
    {
        //$member = DB::table("cms_privileges")->where("name",'Member')->first();
        $data['title'] = 'Register';
        $data['myId'] = CRUDBooster::myId();
        $data['myName'] = CRUDBooster::myName();
        return view('register', $data); //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:cms_users',
            'password' => 'required|string|min:6|confirmed',
            /*'license' => 'required',
            'g-recaptcha-response' => 'required|captcha',*/
        ];
        $member = DB::table("cms_privileges")->where("name", 'Member')->first();
        $this->validate($request, $rules);
        $user1 = new User();
        $user1->name = $request->name;
        $user1->email = $request->email;
        $user1->password = bcrypt($request->password);
        $user1->id_cms_privileges = $member->id;
        $user1->save();
/////////////////////////////////////////////////////
/// login
        $users = DB::table(config('crudbooster.USER_TABLE'))->where("email", $user1->email)->first();

        if (\Hash::check($request->password, $users->password)) {
            $priv = DB::table("cms_privileges")->where("id", $users->id_cms_privileges)->first();

            $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $users->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();

            $photo = ($users->photo) ? asset($users->photo) : asset('vendor/crudbooster/avatar.jpg');
            Session::put('admin_id', $users->id);
            Session::put('admin_is_superadmin', $priv->is_superadmin);
            Session::put('admin_name', $users->name);
            Session::put('admin_photo', $photo);
            Session::put('admin_privileges_roles', $roles);
            Session::put("admin_privileges", $users->id_cms_privileges);
            Session::put('admin_privileges_name', $priv->name);
            Session::put('admin_lock', 0);
            Session::put('theme_color', $priv->theme_color);
            Session::put("appname", CRUDBooster::getSetting('appname'));

            CRUDBooster::insertLog(trans("crudbooster.log_login", ['email' => $users->email, 'ip' => \Illuminate\Support\Facades\Request::server('REMOTE_ADDR')]));

            $cb_hook_session = new \App\Http\Controllers\CBHook;
            $cb_hook_session->afterLogin();

            return redirect('/');
            //return redirect('/')->with('success', 'SUCCESS!!!');
        }
    }

    public function login()
    {
        $validator = Validator::make(\Illuminate\Support\Facades\Request::all(), [
            'email' => 'required|email|exists:' . config('crudbooster.USER_TABLE'),
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->all();

            return redirect()->back()->with(['message' => implode(', ', $message), 'message_type' => 'danger']);
        }

        $email = \Illuminate\Support\Facades\Request::input("email");
        $password = \Illuminate\Support\Facades\Request::input("password");
        $users = DB::table(config('crudbooster.USER_TABLE'))->where("email", $email)->first();

        if (\Hash::check($password, $users->password)) {
            $priv = DB::table("cms_privileges")->where("id", $users->id_cms_privileges)->first();

            $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $users->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();

            $photo = ($users->photo) ? asset($users->photo) : asset('vendor/crudbooster/avatar.jpg');
            Session::put('admin_id', $users->id);
            Session::put('admin_is_superadmin', $priv->is_superadmin);
            Session::put('admin_name', $users->name);
            Session::put('admin_photo', $photo);
            Session::put('admin_privileges_roles', $roles);
            Session::put("admin_privileges", $users->id_cms_privileges);
            Session::put('admin_privileges_name', $priv->name);
            Session::put('admin_lock', 0);
            Session::put('theme_color', $priv->theme_color);
            Session::put("appname", CRUDBooster::getSetting('appname'));

            CRUDBooster::insertLog(trans("crudbooster.log_login", ['email' => $users->email, 'ip' => \Illuminate\Support\Facades\Request::server('REMOTE_ADDR')]));

            $cb_hook_session = new \App\Http\Controllers\CBHook;
            $cb_hook_session->afterLogin();

            return redirect('/');
        } else {
            return redirect('login')->with('message', trans('crudbooster.alert_password_wrong'));
        }

        //return redirect('register')->with('success', 'SUCCESS!!!');
    }

    public function getProfile($id)
    {
        $data['title'] = 'Profile';
        $data['myId'] = CRUDBooster::myId();
        $data['myName'] = CRUDBooster::myName();
        $data['user'] = \Illuminate\Support\Facades\DB::table('cms_users')->
        where('id', $id)->
        first();
        return view('profile.profile', $data);
    }

    public function profile(Request $request, $id)
    {
        if (CRUDBooster::myId() && CRUDBooster::myId() == $id) {
            $rules = [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:cms_users,email,' . $id,
                'password' => 'required|string|min:6|confirmed',
                //'photo' => 'image|mimes:jpeg,bmp,png,jpg|max:2048'
            ];

            if ($request->ph != 1) {
                $rules['photo'] = 'required|image|mimes:jpeg,bmp,png,jpg|max:2048';
                $this->validate($request, $rules);
                $filename = $request->photo->store('public/uploads');
                $data = str_replace('public', '/storage', $filename);

                DB::table('cms_users')->where('id', $id)->update(
                    [
                        'name' => $request->name,
                        'email' => $request->email,
                        'password' => bcrypt($request->password),
                        'photo' => $data,
                    ]
                );
            } else DB::table('cms_users')->where('id', $id)->update(
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),

                ]
            );

            return redirect('/');
        } else
            return redirect('/login');
    }

    public function delProfileImage($id)
    {
        if (CRUDBooster::myId() && CRUDBooster::myId() == $id) {
            DB::table('cms_users')->where('id', $id)->update(
                ['photo' => null,]
            );
            return redirect('/profile/' . $id);
        } else
            return redirect('/login');

    }

    public function getLogout()
    {
        if (CRUDBooster::myId()) {
            $me = CRUDBooster::me();
            CRUDBooster::insertLog(trans("crudbooster.log_logout", ['email' => $me->email]));

            Session::flush();

            return redirect('/')->with('message', trans("crudbooster.message_after_logout"));
        } else
            return redirect('/login');
    }

}
