@extends('layout')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-lg-8">

                <div id="DivIdToPrint" style="font-size: 18px;">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <div class="media-body">
                                <div class="metadata"><span
                                            class="date"> Author:  {{$row->name}}</span>
                                </div>
                            </div>
                        </li>
                        <li class="list-inline-item pull-right">
                            <div class="media-body">
                                <div class="metadata"><span
                                            class="date">{{date('d-m-Y',strtotime($row->created_at))}}</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h2 style="text-align: center">{{$page_title}}</h2>
                    <a href="{{asset(''.$row->image)}}">
                        <p style="text-align: center"><img src="{{asset(''.$row->image)}}"
                                                           style="max-width: 50%; max-height: 300px">
                        </p>
                    </a>
                    {!!($row->body)!!}

                </div>
                <a class="btn btn-default" onclick="like()"><span class="vote plus"><i id="11"
                                                                                       class="fa fa-thumbs-up"> {{$like->lik}}</i></span></a>
                <a class="btn btn-default" onclick="dislike()"><span class="vote minus"><i id="12"
                                                                                           class="fa fa-thumbs-down"> {{$like->dlik}}</i></span></a>
                <span id="slike"></span><input type="hidden" id="like" value="1">
                <span id="sdislike"></span><input type="hidden" id="dislike" value="2">
                {{--<input type="button" onclick="myFunction()" value="comment">--}}
                <hr>
                <div id="comment" style="display: block;">
                    <div id="ccc">
                        <h3>comments</h3>

                        <div class="comments">

                            <ul class="media-list" id="li_comment">
                                @foreach($comments as $comment)
                                    <li class="media">
                                        <div class="media-left" style="margin-right: 20px;">
                                            <a href="#">

                                                @if($comment->photo==null)
                                                    <img src="{{asset('images/avatar.png')}}" alt="Avatar"
                                                         class="avatar media-object img-rounded">
                                                @else
                                                    <img src="{{asset(''.$comment->photo)}}" alt="Avatar"
                                                         class="avatar media-object img-rounded">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading">
                                                <div class="author"> {{$comment->name}}</div>
                                                <div class="metadata">
                                                    <span class="date">{{$comment->date}}</span>
                                                </div>
                                            </div>
                                            <div class="media-text text-justify">{{$comment->comment}}</div>

                                        </div>
                                    </li>

                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <br>
                    <textarea id="textComment" name="comment" rows="5" class="form-control"></textarea>
                    <br>
                    <input type="button" value="comment" class="btn btn-primary" onclick="Message()">
                </div>
                <!----------------------------------------------------------------------------------->
                <!-- Bootstrap 3 -->

                <style>
                    /* CSS */
                    .media-body .author {
                        display: inline-block;
                        font-size: 1rem;
                        color: #000;
                        font-weight: 700;
                    }

                    .media-body .metadata {
                        display: inline-block;
                        margin-left: .5rem;
                        color: #777;
                        font-size: .8125rem;
                    }

                    .footer-comment {
                        color: #777;
                    }

                    .vote.plus:hover {
                        color: #09c708;
                    }

                    .vote.minus:hover {
                        color: red;
                    }

                    .vote {
                        cursor: pointer;
                    }

                    .comment-reply a {
                        color: #777;
                    }

                    .comment-reply a:hover, .comment-reply a:focus {
                        color: #000;
                        text-decoration: none;
                    }

                    .devide {
                        padding: 0px 4px;
                        font-size: 0.9em;
                    }

                    .media-text {
                        margin-bottom: 0.25rem;
                    }

                    .title-comments {
                        font-size: 1.4rem;
                        font-weight: bold;
                        line-height: 1.5rem;
                        color: rgba(0, 0, 0, .87);
                        margin-bottom: 1rem;
                        padding-bottom: .25rem;
                        border-bottom: 1px solid rgba(34, 36, 38, .15);
                    }
                </style>
                <!----------------------------------------------------------------------------------->

            </div>
            <div class="col-md-4">

                <div class="well">
                    <h4>Yangiliklar</h4>
                    <div class="row">

                        <div class="col-lg-12">
                            @foreach($result as $row)
                                <h5><a href='{{url("post/$row->slug")}}' title='{{$row->title}}'>{{$row->title}}</a>
                                </h5>
                                <img src="{{asset(''.$row->image)}}"
                                     style="width: 125px; float: left; padding: 6px">
                                {{str_limit(strip_tags($row->body),130)}}<a href='{{url("post/$row->slug")}}'
                                                                            title='{{$row->title}}'> Батафсил
                                     &raquo;</a>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <div class="media-body">
                                            <div class="metadata"><span
                                                        class="date"> Author: {{$row->name}}</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-inline-item pull-right">
                                        <div class="media-body">
                                            <div class="metadata"><span
                                                        class="date"> Sana: {{date('d-m-Y',strtotime($row->created_at))}}</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <hr style="border-color: #c5c6c8">
                            @endforeach

                            @if(count($result)==0)
                                <div class="alert alert-info">Kechirasiz, bu erda hech narsa topilmadi!</div>
                            @endif
                            {!! $result->links() !!}
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
    <script type="text/javascript">
        function myFunction() {
            var x = document.getElementById("comment");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function Message() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                Type: 'application/json',
                method: 'POST',
                url: '{{url('comment')}}',
                data: {'id_user': {{$myId}}, 'comment': $('#textComment').val(), 'id_post': {{$id}} },
                success: function (data) {
                    if(data.msg=='error')
                        alert('Siz login bulmagansiz!');
                  else {
                        var avatar = "{{asset('images/avatar.png')}}";
                        var asset = "{{asset('')}}";
                        var html = '';
                        html += '<li class="media">';
                        html += '<div class="media-left" style="margin-right: 20px;">';
                        html += '<a href="#">';
                        if (data.photo != null)
                            html += '<img src="' + asset + '' + data.photo + '" alt="Avatar" class="avatar media-object img-rounded">';
                        else
                            html += '<img src="' + avatar + '" alt="Avatar" class="avatar media-object img-rounded">';
                        html += '</a>';
                        html += '</div>';
                        html += '<div class="media-body">';
                        html += '<div class="media-heading">';
                        html += '<div class="author"> ' + data.name + '</div>';
                        html += '<div class="metadata">';
                        html += '<span class="date">' + data.date + '</span>';
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="media-text text-justify">' + data.comment + '</div>';

                        html += '</div>';
                        html += '</li>';
                        $(html).appendTo('#li_comment');
                        document.getElementById('textComment').value = "";
                    }
                    //$("#msg").html(data.msg);
                    //console.log(data);
                },
                error: function (data) {

                }
            });
        }

        function like() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                Type: 'application/json',
                method: 'POST',
                url: '{{url('like')}}',
                data: {'post_id': {{$id}}, 'user_id': {{$myId}},},
                success: function (data) {
                    if(data.msg=='error')
                        alert('Siz login bulmagansiz!');
                   else {
                        document.getElementById("11").innerHTML = ' ' + data.count;
                        if (data.sum) document.getElementById("12").innerHTML = ' ' + data.sum;
                        //$("#msg").html(data.msg);
                    }
                },
                error: function (data) {

                    console.log(data);
                }
            });
        }

        function dislike() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                Type: 'application/json',
                method: 'POST',
                url: '{{url('dislike')}}',
                data: {'post_id': {{$id}}, 'user_id': {{$myId}},},
                success: function (data) {
                    if(data.msg=='error')
                        alert('Siz login bulmagansiz!');
                   else {
                        document.getElementById("12").innerHTML = ' ' + data.count;
                        if (data.sum) document.getElementById("11").innerHTML = ' ' + data.sum;
                        //$("<span>" + data.count + "</span>").appendTo('#sdislike');
                        //$("#msg").html(data.msg);
                        //console.log(data.msg);
                    }
                }, error: function (data) {
                    console.log(data);
                }
            });
        }
    </script>
@endsection