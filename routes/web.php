<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use crocodicstudio\crudbooster\helpers\CRUDBooster;

Route::get('/', 'NewsController@index');
Route::get('/news', 'NewsController@index');

Route::get('/add', 'NewsController@add');
Route::post('/post-save', 'NewsController@insert');

Route::get('/edit/{id}', 'NewsController@edit');
Route::post('/update/{id}', 'NewsController@update');
Route::post('/delete/{id}', 'NewsController@del');
Route::get('post-image/{id}', 'NewsController@delPostImage');
Route::get('post/{slug}', 'NewsController@detail');
Route::post('comment', 'NewsController@comment');
Route::get('myposts', 'NewsController@myPosts');
Route::post('like', 'NewsController@like');
Route::post('dislike', 'NewsController@dislike');

Route::get('/register', 'RegisterController@index');
Route::post('register', 'RegisterController@register');

Route::get('/login', function () {
    $data['title'] = 'Login';
    $data['myName'] = CRUDBooster::myName();
    $data['myId'] = CRUDBooster::myId();
    return view('login', $data);
});
Route::post('login', 'RegisterController@login');

Route::get('/profile/{id}', 'RegisterController@getProfile');
Route::get('image/{id}', 'RegisterController@delProfileImage');
Route::post('profile/{id}', 'RegisterController@profile');

Route::get('logout', 'RegisterController@getLogout');