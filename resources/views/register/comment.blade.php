<style>
/* CSS */
.media-body .author {
display: inline-block;
font-size: 1rem;
color: #000;
font-weight: 700;
}
.media-body .metadata {
display: inline-block;
margin-left: .5rem;
color: #777;
font-size: .8125rem;
}
.footer-comment {
color: #777;
}
.vote.plus:hover {
color: green;
}
.vote.minus:hover {
color: red;
}
.vote {
cursor: pointer;
}
.comment-reply a {
color: #777;
}
.comment-reply a:hover, .comment-reply a:focus {
color: #000;
text-decoration: none;
}
.devide {
padding: 0px 4px;
font-size: 0.9em;
}
.media-text {
margin-bottom: 0.25rem;
}
.title-comments {
font-size: 1.4rem;
font-weight: bold;
line-height: 1.5rem;
color: rgba(0, 0, 0, .87);
margin-bottom: 1rem;
padding-bottom: .25rem;
border-bottom: 1px solid rgba(34, 36, 38, .15);
}
</style>
<!-- Bootstrap 3 -->
<div class="comments">
    <h3 class="title-comments">Комментарии (6)</h3>

    <ul class="media-list">
        <!-- Комментарий (уровень 1) -->
        <li class="media">
            <div class="media-left">
                <a href="#">
                    <img class="media-object img-rounded" src="avatar1.jpg" alt="">
                </a>
            </div>
            <div class="media-body">
                <div class="media-heading">
                    <div class="author">Дима</div>
                    <div class="metadata">
                        <span class="date">16 ноября 2015, 13:43</span>
                    </div>
                </div>
                <div class="media-text text-justify">...</div>
                <div class="footer-comment">
            <span class="vote plus" title="Нравится">
              <i class="fa fa-thumbs-up"></i>
            </span>
                    <span class="rating">+1</span>
                    <span class="vote minus" title="Не нравится">
              <i class="fa fa-thumbs-down"></i>
            </span>
                    <span class="devide">|</span>
                    <span class="comment-reply">
              <a href="#" class="reply">ответить</a>
            </span>
                </div>
                <!-- Вложенный медиа-компонент (уровень 2) -->
                <div class="media">
                    <div class="media-left">...</div>
                    <div class="media-body">
                        <div class="media-heading">...</div>
                        <div class="media-text text-justify">...</div>
                        <div class="footer-comment">...</div>
                        <!-- Вложенный медиа-компонент (уровень 3) -->
                        <div class="media">
                            ...
                        </div><!-- Конец вложенного комментария (уровень 3) -->
                    </div>
                </div><!-- Конец вложенного комментария (уровень 2) -->
                <!-- Ещё один вложенный медиа-компонент (уровень 2) -->
                <div class="media">
                    ...
                </div><!-- Конец ещё одного вложенного комментария (уровень 2) -->
            </div>
        </li><!-- Конец комментария (уровень 1) -->
        <!-- Комментарий (уровень 1) -->
        <li class="media">
            ...
        </li><!-- Конец комментария (уровень 1) -->
        <!-- Комментарий (уровень 1) -->
        <li class="media">
            ...
        </li><!-- Конец комментария (уровень 1) -->
    </ul>
</div>