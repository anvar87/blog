-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июл 02 2020 г., 13:57
-- Версия сервера: 10.4.8-MariaDB
-- Версия PHP: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `laravel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responses` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_attachments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2020-06-27 07:18:45', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-27 07:19:07', NULL),
(2, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-28 02:04:00', NULL),
(3, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-28 02:05:21', NULL),
(4, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-28 02:05:29', NULL),
(5, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', 'anvar@anvar.an logout', '', 2, '2020-06-28 02:08:38', NULL),
(6, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-28 02:13:12', NULL),
(7, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-28 02:59:23', NULL),
(8, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-28 02:59:34', NULL),
(9, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-28 03:20:28', NULL),
(10, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-28 03:20:34', NULL),
(11, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-28 04:04:27', NULL),
(12, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-28 10:26:31', NULL),
(13, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:01:34', NULL),
(14, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:01:54', NULL),
(15, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:03:20', NULL),
(16, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:03:57', NULL),
(17, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:04:02', NULL),
(18, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:13:10', NULL),
(19, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:13:16', NULL),
(20, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:24:20', NULL),
(21, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:24:29', NULL),
(22, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:30:54', NULL),
(23, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:33:35', NULL),
(24, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:34:12', NULL),
(25, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:34:30', NULL),
(26, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:34:47', NULL),
(27, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:35:22', NULL),
(28, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:41:14', NULL),
(29, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:41:25', NULL),
(30, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 00:41:56', NULL),
(31, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-06-29 00:42:16', NULL),
(32, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 00:42:25', NULL),
(33, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 00:57:31', NULL),
(34, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 00:58:01', NULL),
(35, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-06-29 01:12:22', NULL),
(36, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 01:12:34', NULL),
(37, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 01:25:46', NULL),
(38, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-06-29 01:40:35', NULL),
(39, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 01:41:01', NULL),
(40, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 02:00:26', NULL),
(41, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-06-29 02:36:17', NULL),
(42, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 02:36:26', NULL),
(43, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-06-29 02:37:04', NULL),
(44, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 03:21:40', NULL),
(45, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', ' logout', '', NULL, '2020-06-29 09:10:08', NULL),
(46, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-06-29 09:30:16', NULL),
(47, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/admin/logout', ' logout', '', NULL, '2020-06-29 09:31:34', NULL),
(48, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 10:35:48', NULL),
(49, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-29 12:36:30', NULL),
(50, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-29 12:36:39', NULL),
(51, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 01:41:07', NULL),
(52, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 01:43:50', NULL),
(53, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.2.195 Yowser/2.5 Yptp/1.23 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:02:29', NULL),
(54, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:03:43', NULL),
(55, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-30 08:23:10', NULL),
(56, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:23:16', NULL),
(57, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-30 08:34:41', NULL),
(58, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:34:48', NULL),
(59, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-30 08:42:03', NULL),
(60, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:43:06', NULL),
(61, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.2.195 Yowser/2.5 Yptp/1.23 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-30 08:45:18', NULL),
(62, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.2.195 Yowser/2.5 Yptp/1.23 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:45:26', NULL),
(63, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.2.195 Yowser/2.5 Yptp/1.23 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-30 08:50:40', NULL),
(64, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.2.195 Yowser/2.5 Yptp/1.23 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:50:48', NULL),
(65, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-30 08:58:02', NULL),
(66, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 08:58:08', NULL),
(67, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-06-30 09:04:08', NULL),
(68, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-06-30 09:04:17', NULL),
(69, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-01 01:43:15', NULL),
(70, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-01 04:29:29', NULL),
(71, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-01 06:01:42', NULL),
(72, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-01 06:01:50', NULL),
(73, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-01 06:40:08', NULL),
(74, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-01 06:40:15', NULL),
(75, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-01 06:42:00', NULL),
(76, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-01 06:42:07', NULL),
(77, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-01 07:01:17', NULL),
(78, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-01 07:01:24', NULL),
(79, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-01 11:40:19', NULL),
(80, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-01 13:34:56', NULL),
(81, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-01 13:35:04', NULL),
(82, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', ' logout', '', NULL, '2020-07-02 00:24:04', NULL),
(83, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 00:31:19', NULL),
(84, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 00:44:26', NULL),
(85, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 00:44:33', NULL),
(86, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 00:45:52', NULL),
(87, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-02 00:51:09', NULL),
(88, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 00:51:17', NULL),
(89, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 00:56:40', NULL),
(90, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 00:56:53', NULL),
(91, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-02 03:22:37', NULL),
(92, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 03:22:42', NULL),
(93, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-02 03:35:35', NULL),
(94, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 03:35:42', NULL),
(95, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 03:58:04', NULL),
(96, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/register', 'sardor@sardor.sa login with IP Address ::1', '', 3, '2020-07-02 03:58:42', NULL),
(97, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'sardor@sardor.sa logout', '', 3, '2020-07-02 04:00:18', NULL),
(98, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 04:00:24', NULL),
(99, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 04:00:52', NULL),
(100, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'sardor@sardor.sa login with IP Address ::1', '', 3, '2020-07-02 04:01:12', NULL),
(101, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'sardor@sardor.sa logout', '', 3, '2020-07-02 04:03:54', NULL),
(102, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/register', 'nigora@nigora.ni login with IP Address ::1', '', 4, '2020-07-02 04:04:23', NULL),
(103, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'nigora@nigora.ni logout', '', 4, '2020-07-02 04:07:08', NULL),
(104, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'nigora@nigora.ni login with IP Address ::1', '', 4, '2020-07-02 04:07:26', NULL),
(105, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'nigora@nigora.ni logout', '', 4, '2020-07-02 05:32:02', NULL),
(106, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 05:32:10', NULL),
(107, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-02 05:32:19', NULL),
(108, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 05:32:26', NULL),
(109, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 06:37:12', NULL),
(110, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 06:40:51', NULL),
(111, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 06:42:01', NULL),
(112, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 06:42:42', NULL),
(113, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 06:42:49', NULL),
(114, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 06:43:09', NULL),
(115, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-02 06:43:20', NULL),
(116, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'admin@crudbooster.com login with IP Address ::1', '', 1, '2020-07-02 06:43:28', NULL),
(117, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'admin@crudbooster.com logout', '', 1, '2020-07-02 06:44:12', NULL),
(118, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 06:44:21', NULL),
(119, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 06:51:33', NULL),
(120, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 06:54:15', NULL),
(121, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/logout', 'anvar@anvar.an logout', '', 2, '2020-07-02 06:55:15', NULL),
(122, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'http://localhost/blog/public/login', 'anvar@anvar.an login with IP Address ::1', '', 2, '2020-07-02 06:55:50', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_dashboard` tinyint(1) NOT NULL DEFAULT 0,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2020-06-27 07:18:43', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2020-06-27 07:18:43', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2020-06-27 07:18:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2020-06-27 07:18:43', NULL),
(2, 'Member', 0, 'skin-blue', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2020-06-27 07:18:43', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2020-06-27 07:18:43', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2020-06-27 07:18:43', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2020-06-27 07:18:43', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2020-06-27 07:18:44', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2020-06-27 07:18:44', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2020-06-27 07:18:44', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2020-06-27 07:18:44', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2020-06-27 07:18:44', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2020-06-27 07:18:45', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2020-06-27 07:18:45', NULL),
(12, 1, 1, 1, 1, 1, 2, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2020-06-27 07:18:45', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2020-06-27 07:18:45', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2020-06-27 07:18:45', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2020-06-27 07:18:45', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'CRUDBooster', 'text', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2020-06-27 07:18:45', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', '', 'upload_image', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', '', 'upload_image', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2020-06-27 07:18:45', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', '', 'text', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', '', 'text', NULL, NULL, '2020-06-27 07:18:45', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', NULL, 'admin@crudbooster.com', '$2y$10$P7rWggR3wmLf.IFj8qLS9evpPBDnS7J9r637yrcfjcsAT4m3PcdH6', 1, '2020-06-27 07:18:43', NULL, 'Active'),
(2, 'Anvar', '/storage/uploads/Ay392K3Lj0LvFLWLe1tHxdInLMN4g6hQXE06NItT.jpeg', 'anvar@anvar.an', '$2y$10$S6pCIViSl9G1gAFUcjz3AeL6aWeZZdH3Mu6BtlinigBX86w8BJxX.', 2, '2020-06-28 01:47:24', '2020-06-28 01:47:24', NULL),
(3, 'Sardor', '/storage/uploads/wSWt0DbTtbpzieC2F3Gg1KIc26uay3umOV0oXJsc.jpeg', 'sardor@sardor.sa', '$2y$10$93QI6TDuFVXD/stoHdP2ZuqFQ4YCjL3MGZAMnOFdxH/sarRg9KlN.', 2, '2020-07-02 03:58:41', '2020-07-02 03:58:41', NULL),
(4, 'Nigora', '/storage/uploads/gZJznj3UcYFLN6HyeOwyd4svIQzp8PVLxoEIa8BX.jpeg', 'nigora@nigora.ni', '$2y$10$y4OrwbTCam3MBK4VdzpbJuJPYWiFy4bHMQDZRVwxgLNE1YPddkQGS', 2, '2020-07-02 04:04:23', '2020-07-02 04:04:23', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_post` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `like` int(11) NOT NULL DEFAULT 0,
  `dislike` int(11) NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `id_post`, `id_user`, `like`, `dislike`, `parent_id`, `comment`, `created_at`, `updated_at`) VALUES
(35, 1, 1, 0, 0, 0, 'test', '2020-07-02 08:22:31', NULL),
(36, 4, 1, 0, 0, 0, 'tttt', '2020-07-02 08:24:04', NULL),
(37, 4, 1, 0, 0, 0, 'twtwt', '2020-07-02 08:29:52', NULL),
(38, 4, 1, 0, 0, 0, 'qani', '2020-07-02 08:34:38', NULL),
(39, 4, 1, 0, 0, 0, 'gg', '2020-07-02 08:35:00', NULL),
(40, 1, 1, 0, 0, 0, 'yaxshi', '2020-07-02 08:35:29', NULL),
(41, 4, 2, 0, 0, 0, 'gap yuq', '2020-07-02 08:35:56', NULL),
(42, 4, 2, 0, 0, 0, 'qani', '2020-07-02 08:37:16', NULL),
(43, 1, 2, 0, 0, 0, 'bunisini kuraychi', '2020-07-02 08:38:31', NULL),
(44, 4, 3, 0, 0, 0, 'llll', '2020-07-02 08:59:01', NULL),
(45, 5, 4, 0, 0, 0, 'ffj', '2020-07-02 09:34:52', NULL),
(46, 4, 4, 0, 0, 0, '!!!', '2020-07-02 10:31:11', NULL),
(47, 5, 2, 0, 0, 0, 'hhhh', '2020-07-02 10:49:55', NULL),
(48, 5, 2, 0, 0, 0, 'jasldk', '2020-07-02 11:41:19', NULL),
(49, 6, 1, 0, 0, 0, 'sajk', '2020-07-02 11:43:59', NULL),
(50, 6, 2, 0, 0, 0, 'ewiuo', '2020-07-02 11:44:30', NULL),
(51, 6, 2, 0, 0, 0, 'bk', '2020-07-02 11:54:26', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `like` tinyint(1) NOT NULL,
  `dislike` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`, `like`, `dislike`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 0, 1, '2020-07-01 12:42:10', '2020-07-01 13:35:19'),
(2, 3, 1, 1, 0, '2020-07-01 16:42:22', NULL),
(3, 4, 2, 1, 0, '2020-07-02 05:44:03', NULL),
(4, 4, 1, 1, 0, '2020-07-02 07:19:29', NULL),
(5, 1, 1, 0, 1, '2020-07-02 07:46:52', '2020-07-02 07:47:06'),
(6, 5, 4, 1, 0, '2020-07-02 10:23:14', '2020-07-02 10:26:00'),
(7, 5, 2, 1, 0, '2020-07-02 10:49:37', '2020-07-02 11:19:15'),
(8, 6, 1, 1, 0, '2020-07-02 11:43:54', NULL),
(9, 6, 2, 1, 0, '2020-07-02 11:44:27', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_08_07_145904_add_table_cms_apicustom', 1),
(2, '2016_08_07_150834_add_table_cms_dashboard', 1),
(3, '2016_08_07_151210_add_table_cms_logs', 1),
(4, '2016_08_07_151211_add_details_cms_logs', 1),
(5, '2016_08_07_152014_add_table_cms_privileges', 1),
(6, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(7, '2016_08_07_152320_add_table_cms_settings', 1),
(8, '2016_08_07_152421_add_table_cms_users', 1),
(9, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(10, '2016_08_07_154624_add_table_cms_moduls', 1),
(11, '2016_08_17_225409_add_status_cms_users', 1),
(12, '2016_08_20_125418_add_table_cms_notifications', 1),
(13, '2016_09_04_033706_add_table_cms_email_queues', 1),
(14, '2016_09_16_035347_add_group_setting', 1),
(15, '2016_09_16_045425_add_label_setting', 1),
(16, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(17, '2016_10_01_141740_add_method_type_apicustom', 1),
(18, '2016_10_01_141846_add_parameters_apicustom', 1),
(19, '2016_10_01_141934_add_responses_apicustom', 1),
(20, '2016_10_01_144826_add_table_apikey', 1),
(21, '2016_11_14_141657_create_cms_menus', 1),
(22, '2016_11_15_132350_create_cms_email_templates', 1),
(23, '2016_11_15_190410_create_cms_statistics', 1),
(24, '2016_11_17_102740_create_cms_statistic_components', 1),
(25, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(26, '2020_06_29_075731_create_posts_table', 2),
(27, '2020_06_29_080011_create_comments_table', 2),
(28, '2020_07_01_083231_create_likes_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `like` int(11) NOT NULL DEFAULT 0,
  `dislike` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `image`, `body`, `user_id`, `like`, `dislike`, `created_at`, `updated_at`) VALUES
(1, '19-Mavzu: Ma\'lumotlarni yig’ish va LabVIEW uskunalarini boshqarish', '19-mavzu-malumotlarni-yigish-va-labview-uskunalarini-boshqarish', '/storage/uploads/XbQHJJ9LOuRCyqPayxig0AkaDeoXBcOgLcGjcqW1.jpeg', '<p><strong>Yuzadagi panel kiritish/chiqarish palitrasini o&rsquo;rganish: ostsillogramma ma\'lumotlari turi, ma\'lumotlarni yig&rsquo;ish kannallari nomlari</strong><strong>.</strong></p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>LabVIEW o</strong><strong>lami</strong><strong>.</strong></p>\r\n<p><strong>Reja:</strong></p>\r\n<ol>\r\n<li>LabVIEW ning asosiy konse&rsquo;tsiyasi, uning imkoniyatlari.</li>\r\n<li>Strukturaviy sxema</li>\r\n<li>Diagrammalarni taxrir qilish darchasi</li>\r\n</ol>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>Tayanch so\'z va iboralar: </strong>Blank, VI, Front &lsquo;anel;</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>Old &lsquo;anel</strong></p>', 1, 0, 0, '2020-06-29 13:08:31', '2020-06-29 13:08:31'),
(4, 'Struktura sxemasida obyektlarni almashtirish va qo‟shish', 'struktura-sxemasida-obyektlarni-almashtirish-va-qoshish', '/storage/uploads/yBYkxDJNxbeZkgcOSyoxPnJI59NeeEogrqBYoebu.jpeg', '<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Misol uchun siz struktura sxemasida Increment funksiyasidan foydalanaya&rsquo;siz. Lakin siz Decrement funksiyasidan foydalanishiz kerek edi. Siz Functions &lsquo;alitrasidan foydalanib Increment funksiya bog‟ini o‟chirib tashlashingiz mumkin,keyin Decrement funksiya bog‟ini tanlaysiz va ularni qo‟shib qo‟yasiz. Siz ya‟na obyekt menyusidan Re&rsquo;lase sozlagichini tanlab Functions &lsquo;alitrasini chaqirshingiz mumkin. Va Decrement funksiyasini tanlaysiz. Bu oarqali LabView eski bo‟g‟ , tugun turgan joyga yangi tugunni qo‟yishni oldini oladi. Va hech qanday xatolik vujudga kelmaydi. Siz funksiyalarni hatr xilga almashtirishingiz mumkin. Faqat terminallar raqami va ma‟lumotlar toifasi har bir funksional tugunda harxil bo‟lishi kerak. Yo‟qsa siz uzilgan bog‟lanishga ega bo‟lib qolishingiz mumkin.</p>\r\n<p>&nbsp;Siz ya‟na Re&rsquo;lace dan foydalanib, bir konstantani boshqa bir konstantaga yoki strukturani boshqa bir o‟xshash strukturaga alishtirishingix mukin. Misol uchun While Loo&rsquo; (hozircha sikl) ni For Loo&rsquo; ( sikl uchun) ga. O‟tkazgichning obyekt menyusida Insert (qo‟yish) bo‟limi mavjud. Insert bo‟limini tanlasangiz Functions &lsquo;alitrasiga chiqasiz va bu yerda siz hoxlagan funksiyangizni tanlaysiz. Siz o‟tkazgich orqali menyuga murojat qilganiz uchun LabView tugunlarni ulaydi. Lekin siz hushyorro bo‟lishingiz kerek. Chunki tugunda bir qancha kirish va chiqishga ega terminal bo‟lib qolsa simlar siz kutgan terminalga emas aksincha boshqa terminalga ulanib qolishi mumkin.</p>', 1, 0, 0, '2020-07-02 00:41:01', '2020-07-02 00:41:01'),
(5, '23-Mavzu: VISA- uskunalarni boshqarish uchun uskunalarni guruxl bilan tanishish.', '23-mavzu-visa-uskunalarni-boshqarish-uchun-uskunalarni-guruxl-bilan-tanishish', '/storage/uploads/3ehqtxgg1mdTGUBzDS9ribHALxyK6ji8YAAAUzOm.jpeg', '<p>&nbsp;VISA resurslarini yaratish va sozlash. Uskunalarni boshqarish yordamchisidan foydalanishni o&rsquo;rganish. Uskunalarni boshqarish uchun VISA VU ni o&rsquo;rganish.</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>Reja:</strong></p>\r\n<ol>\r\n<li>LabVIEW ning asosiy dasturlash &lsquo;rintsi&rsquo;larini.</li>\r\n</ol>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>Tayanch so\'z va iboralar: </strong>komoanentlari, grafik dasturlash, Rizual bo&rsquo;lmagan elementlar</p>\r\n<p>&nbsp;</p>\r\n<p>Virtual anjomlar ba&rsquo;zas ida tizimlarni loyihalash asoslari.</p>\r\n<p>Amaliy misollarni ado etishga kirishishdan oldin LabVIEW muhitida tizimlarni ishlab chiqishni nazariy as&rsquo;ektlarini ba&rsquo;zilarini qisqacha ko&rsquo;rib chiqamiz. Juda quvvatli va loyihalashni ko&rsquo;&rsquo; funksiyali tizimi LabVIEW muhandisga juda oddiy va intuitiv tushunarli interfeysni ishlab chiqishda bir qancha fundamental namoish etishlarga tayanadi. Avvalda aytilganidek LabVIEW ni har bir ilovasi virtual anjomdan (V1) iborat. Ilova tarkibiga grafik (vizual) boshqaruv elementlari (komoanentlari) va nazoratni shuningdek vizual bo&rsquo;lmagan elemantlarni komoanentlarilar o&rsquo;z ichiga olishi mumkin. Rizual bo&rsquo;lmagan elementlar qandaydir funksiyani ado etadi (matematik, mantiqiy, o&rsquo;zgartirish va signallarni generatsiya qilish va boshqalar).</p>', 4, 0, 0, '2020-07-02 04:28:01', '2020-07-02 04:28:01'),
(6, 'LabVIEW muhitida muhandislik masalalarini yechishni amaliy misollari.', 'labview-muhitida-muhandislik-masalalarini-yechishni-amaliy-misollari', '/storage/uploads/BgxqpKwGSovhAkzrEgYKuax1eZScJ2lHpRM44LoF.jpeg', '<p>LabVIEW tizimini loyihalash asoslarini o&rsquo;rganish uchun bir qator dasturlash misollarini ko&rsquo;rib chiqamiz. Dasturlash asosini va virtual namoish anjomlarini ishlab chiqishda signal manbai sifatida LabVIEW tizimini vizual bo&rsquo;lmagan elemantlaridan (signal simulyatorlaridan) foydalaniladi.</p>\r\n<p>Birinchi misolda virtual anjomni yaratilishi ko&rsquo;rsatiladi, u simulyator signalini ishlovini amalga oshiradi va uni natijalarini aks ettiruvchi virtual qurilmaga (grafik indekatorga chiqaradi).</p>\r\n<ul>\r\n<li>Har bir virtual tizimni kirishiga to&rsquo;g&rsquo;ri burchak shaklidagi chastota dio&rsquo;azoni 100 dan 1000 Gs gacha va am&rsquo;letudasi 1V bo&rsquo;lgan signal beriladi deb faraz qilamiz. Shuningdek faraz qilamiz chiqishdagi signal uchburchak shakliga ega bo&rsquo;lib uning am&rsquo;letuda koeffitsenti 0,33 va faza siljishi kirayotgan signalga nisbatan 180<sup>o</sup> bo&rsquo;ladi. Ikkala signal ham egri chiziq shaklida bitta grafikda aks etadi.</li>\r\n</ul>', 2, 0, 0, '2020-07-02 05:33:12', '2020-07-02 05:33:12');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT для таблицы `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
