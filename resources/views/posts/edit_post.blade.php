@extends('layout')
@section('content')
    <!-- Your Page Content Here -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li style="margin-left: 10px">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                <h2 class="login-box-msg">Edit post
                </h2>
                <form action="{{ url('update/'.$row->id) }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label for="exampleInputName">Title</label><span class="text-danger"
                                                                         title="This field is required">*</span>
                        <input type="text" class="form-control" id="name" name="title" value="{{$row->title}}"
                               aria-describedby="nameHelp" placeholder="Enter name">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Body</label><span class="text-danger"
                                                                          title="This field is required">*</span>
                        <textarea name="body"
                                  class="form-control"
                                  rows="5">{{$row->body}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputImage">Photo </label>
                        @if($row->image == null)
                            <span class="text-danger" title="This field is required">*</span>
                            <input type="file" class="form-control" name="photo">

                        @else
                            <img src="{{asset(''.$row->image)}}" width="100px">
                            <input type="hidden" name="ph" value="1">
                            <a class="btn btn-danger" href="{{url('post-image/'.$row->id)}}">X</a>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>

            </div>
        </div>
    </div><!--END AUTO MARGIN-->
    <script src="{{asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
    <!-- <script src="/path/to/tinymce/jquery.tinymce.min.js"></script> -->
    <script type="text/javascript">
        tinymce.init({

            selector: 'textarea',  // change this value according to your HTML
            theme: 'modern',
            height: 300,
            //relative_urls: false,
            plugins: [
                'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'save table contextmenu directionality emoticons template paste textcolor'
            ],

            // content_css: '{{asset('vendor/crudbooster/assets/bootstrap/css/bootstrap.min.css')}}',
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
        });
    </script>


@endsection