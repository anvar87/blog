@extends('layout')
@section('content')
    <!------  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"
          id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    Include the above in your HEAD tag ---------->
    <div class="container">
        <div class="row">

            <div class="col-lg-12">
                @if (\Session::has('message'))
                    <div class="alert alert-warning">
                        <p>{{ \Session::get('message') }}</p>
                    </div>
                @endif

                <h2 class="login-box-msg">Login
                </h2>
                <form action="{{ url('login') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label><span class="text-danger" title="This field is required">*</span>
                        <input type="email" class="form-control" name="email" value="{{old('email')}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        @if ($errors->has('email'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label><span class="text-danger" title="This field is required">*</span>
                        <input type="password" class="form-control" name='password' id="exampleInputPassword1" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
            </div>
        </div>
    </div>
@endsection