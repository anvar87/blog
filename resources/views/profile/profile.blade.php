@extends('layout')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-lg-12">
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif

                <h2 class="login-box-msg">Profile
                </h2>
                <form action="{{ url('profile/'.$user->id) }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label for="exampleInputName">Name</label><span class="text-danger" title="This field is required">*</span>
                        <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}"
                               aria-describedby="nameHelp" placeholder="Enter name">
                        @if ($errors->has('name'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label><span class="text-danger" title="This field is required">*</span>
                        <input type="email" class="form-control" name="email" value="{{$user->email}}"
                               id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        @if ($errors->has('email'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputImage">Photo </label>
                        @if($user->photo == null)
                            <span class="text-danger" title="This field is required">*</span>
                            <input type="file" class="form-control" name="photo">
                            @if ($errors->has('photo'))
                                <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                            @endif
                        @else
                            <img src="{{asset(''.$user->photo)}}" width="100px">
                            <input type="hidden" name="ph" value="1">
                            <a class="btn btn-danger" href="{{url('image/'.$myId)}}">X</a>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label><span class="text-danger" title="This field is required">*</span>
                        <input type="password" class="form-control" name='password' id="exampleInputPassword1"
                               placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Confirm password</label>
                        <input type="password" class="form-control" name='password_confirmation'
                               placeholder="Confirm password">
                    </div>

                    <button type="submit" class="btn btn-primary">Update profile</button>
                </form>
            </div>
        </div>
    </div>
@endsection