@extends('layout')
@section('content')
    <!-- Your Page Content Here -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li style="margin-left: 10px">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                <h2 class="login-box-msg">Posts
                </h2>
                <a href="{{url('add')}}" class="btn btn-success" style="float: right"> Add</a>
                <br> <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Image</th>
                        <th scope="col">Title</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>

                    <tbody id="tr">
                    @foreach($row as $item)
                        <tr>
                            <th scope="row">{{$item->id}}</th>
                            <td><img src="{{asset(''.$item->image)}}" width="100px"></td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>
                                <a href="{{url('edit/'.$item->id)}}" class="btn btn-default"><i class="fa fa-edit"> </i></a>
                                <a class="btn btn-default" onclick="del({{$item->id}})"><i class="fa fa-trash"> </i></a>
                                {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div><!--END AUTO MARGIN-->
    <script type="text/javascript">
        function del(id) {

            var url = '{{url('delete')}}/' + id;
            bootbox.confirm({
                size: "small",
                message: "Are you sure?",
                callback: function (result) { /* result is a boolean; true = OK, false = Cancel*/
                    if (result) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'application/json',
                            url: url,
                            method: 'post',
                            data: id,
                            success: function (data) {
                                if(data.msg == 'error')
                                    alert('Siz login bulmagansiz!');
                                var html = '';
                                var urll = '{{url('edit')}}' + '/';
                                var img = '{{asset('')}}';
                                for (var i = 0; i < data.rows.length; i++) {
                                    html += '<tr>';
                                    html += '<th scope = "row">' + data.rows[i].id + '</th>';
                                    html += '<td><img src = "' + '' + img + '' + data.rows[i].image + '" width = "100px"></td>';
                                    html += '<td>' + data.rows[i].title + '</td>';
                                    html += '<td>' + data.rows[i].created_at + '</td>';
                                    html += '<td><a href = "' + '' + urll + '' + data.rows[i].id + '" class = "btn btn-default"><i class = "fa fa-edit"></i></a>';
                                    html += '<a class = "btn btn-default" onclick = "del(' + data.rows[i].id + ')"><i class = "fa fa-trash"></i></a>';
                                    html += '</td></tr>';
                                }
                                document.getElementById("tr").innerHTML = html;
                                //console.log(html);
                            }, error: function (error) {

                            }
                        });
                    }
                }
            });
        }
    </script>

@endsection