<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Posts;
use Carbon\Carbon;
use crocodicstudio\crudbooster\commands\CrudboosterInstallationCommand;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function index()
    {
        $data['title'] = 'News';

        $data['myName'] = CRUDBooster::myName();
        $data['myId'] = CRUDBooster::myId() ? CRUDBooster::myId() : 0;
        $data['photo'] = CRUDBooster::myPhoto();

        $result = DB::table('posts')
            ->join('cms_users', 'cms_users.id', '=', 'posts.user_id')
            ->select('posts.*', 'cms_users.name')
            ->orderby('posts.id', 'desc')
            ->paginate(5);
        $data['result'] = $result;
        $data['page_title'] = $result[0]->title;
        $data['page_description'] = str_limit(strip_tags($result[0]->body), 155);
        $data['row'] = $result[0];
        $data['id'] = $result[0]->id;
        $data['comments'] = DB::table('comments')
            ->join('cms_users', 'cms_users.id', '=', 'comments.id_user')
            ->select('cms_users.*', 'comments.comment', 'comments.created_at as date')
            ->where('id_post', $result[0]->id)
            //->orderby('id', 'desc')
            ->get();
        $l = DB::select('select SUM(likes.like) as lik, SUM(likes.dislike) as dlik from likes WHERE post_id=' . $result[0]->id . '');
        $data['like'] = $l[0];
        return view('posts', $data); //
    }

    public function myPosts()
    {
        if (CRUDBooster::myId()) {
            $data['title'] = 'My News';
            $data['myName'] = CRUDBooster::myName();
            $data['myId'] = CRUDBooster::myId();
            $row = DB::table('posts')
                ->select('*')
                ->where('posts.user_id', CRUDBooster::myId())
                ->orderby('posts.id', 'desc')->get();
            $data['row'] = $row;
            return view('posts.my_posts', $data); //
        } else
            return redirect('/login');
    }

    public function add()
    {
        if (CRUDBooster::myId()) {
            $data['title'] = 'Add News';
            $data['myName'] = CRUDBooster::myName();
            $data['myId'] = CRUDBooster::myId();
            return view('posts.add_post', $data); //
        } else
            return redirect('/login');
    }

    public function insert(Request $request)
    {
        if (CRUDBooster::myId()) {
            $data['title'] = 'Add News';
            $data['myName'] = CRUDBooster::myName();
            $data['myId'] = CRUDBooster::myId();
            $rules = [
                'title' => 'required|string|max:255',
                'body' => 'required|min:3',
                'photo' => 'required|image|mimes:jpeg,bmp,png,jpg|max:2048',
            ];

            $this->validate($request, $rules);
            $filename = $request->photo->store('public/uploads');
            $data = str_replace('public', '/storage', $filename);
            $user1 = new Posts();
            $user1->title = $request->title;
            $user1->body = $request->body;
            $user1->image = $data;
            $user1->slug = str_slug($request->title);
            $user1->user_id = CRUDBooster::myId();
            $user1->save();

            return redirect('/'); //
        } else
            return redirect('/login');
    }

    public function edit($id)
    {
        if (CRUDBooster::myId()) {
            $data['title'] = 'Edit News';
            $data['myName'] = CRUDBooster::myName();
            $data['myId'] = CRUDBooster::myId();
            $data['row'] = DB::table('posts')->where('id', $id)->first();
            return view('posts.edit_post', $data); //
        } else
            return redirect('/login');
    }

    public function update(Request $request, $id)
    {
        if (CRUDBooster::myId()) {
            $data['title'] = 'Edit News';
            $data['myName'] = CRUDBooster::myName();
            $data['myId'] = CRUDBooster::myId();
            $rules = [
                'title' => 'required|string|max:255',
                'body' => 'required|min:3',
                //'photo' => 'required|image|mimes:jpeg,bmp,png,jpg|max:2048',
            ];
            if ($request->ph != 1) {
                $rules['photo'] = 'required|image|mimes:jpeg,bmp,png,jpg|max:2048';
                $this->validate($request, $rules);
                $filename = $request->photo->store('public/uploads');
                $data = str_replace('public', '/storage', $filename);

                DB::table('posts')->where('id', $id)->update(
                    [
                        'title' => $request->title,
                        'slug' => str_slug($request->title),
                        'body' => $request->body,
                        'image' => $data,
                    ]
                );
            } else DB::table('posts')->where('id', $id)->update(
                [
                    'title' => $request->title,
                    'slug' => str_slug($request->title),
                    'body' => $request->body,
                ]
            );

            return redirect('/');
        } else
            return redirect('/login');
    }

    public function del($id)
    {
        if (CRUDBooster::myId()) {
            DB::table('posts')->where('id', $id)->delete();
            $data['row'] = DB::table('posts')
                ->orderby('posts.id', 'desc')->get();
            return response()->json(['msg' => 'success!', 'rows' => $data['row']]);
        } else
            return response()->json(['msg' => 'error']);
    }

    public function detail($slug)
    {
        if (CRUDBooster::myId()) {
            $data['title'] = 'Post';
            $data['myName'] = CRUDBooster::myName();
            $data['myId'] = CRUDBooster::myId();
            $data['photo'] = CRUDBooster::myPhoto();
            $row = DB::table('posts')
                ->join('cms_users', 'cms_users.id', '=', 'posts.user_id')
                ->select('posts.*', 'cms_users.name')
                ->where('posts.slug', $slug)
                ->first();

            $result = DB::table('posts')
                ->join('cms_users', 'cms_users.id', '=', 'posts.user_id')
                ->select('posts.*', 'cms_users.name')
                ->orderby('posts.id', 'desc')
                ->paginate(5);
            $data['row'] = $row;
            $data['page_title'] = $row->title;
            $data['page_description'] = str_limit(strip_tags($row->body), 155);
            $data['result'] = $result;
            $data['id'] = $row->id;
            $data['comments'] = DB::table('comments')
                ->join('cms_users', 'cms_users.id', '=', 'comments.id_user')
                ->select('cms_users.*', 'comments.comment', 'comments.created_at as date')
                ->where('id_post', $row->id)
                //->orderby('id', 'desc')
                ->get();
            $l = DB::select('select SUM(likes.like) as lik, SUM(likes.dislike) as dlik from likes WHERE post_id=' . $row->id . '');
            $data['like'] = $l[0];
            return view('posts', $data);
        } else
            return redirect('/login');
    }

    public function delPostImage($id)
    {
        if (CRUDBooster::myId() && CRUDBooster::myId() == $id) {
            DB::table('posts')->where('id', $id)->update(
                ['image' => null,]
            );
            return redirect('/edit/' . $id);
        } else
            return redirect('/login');
    }

    public function comment(Request $request)
    {
        if (CRUDBooster::myId()) {
            $data['title'] = 'Post';
            $data['myName'] = CRUDBooster::myName();
            $data['myId'] = CRUDBooster::myId();
            $data['photo'] = CRUDBooster::myPhoto();

            $req = $request->all();
            $msg = "Success!";
            DB::table('comments')->insert(
                [
                    'id_user' => $request->id_user,
                    'id_post' => $request->id_post,
                    'comment' => $request->comment,
                    'created_at' => Carbon::now('Asia/Tashkent'),
                ]
            );
            $row = DB::table('comments')
                ->join('cms_users', 'cms_users.id', '=', 'comments.id_user')
                ->select('cms_users.photo', 'cms_users.name', 'comments.comment', 'comments.created_at')
                ->where('id_post', $request->id_post)
                ->where('id_user', $request->id_user)
                ->orderby('comments.id', 'desc')
                ->first();
            return response()->json(array('photo' => $row->photo, 'name' => $row->name, 'date' => $row->created_at, 'comment' => $request->comment), 200);
        } else
            return response()->json(['msg' => 'error']);
    }

    public function like(Request $request)
    {
        if (CRUDBooster::myId()) {
            //$req = $request->all();
            $msg = "Success!";
            $result = DB::table('likes')
                ->where('user_id', $request->user_id)
                ->where('post_id', $request->post_id)
                ->first();
            if ($result) {
                if ($result->like == 0) {
                    $fr = DB::table('likes')->where('id', '=', $result->id)
                        ->update(
                            [
                                'like' => 1,
                                'dislike' => 0,
                                'updated_at' => Carbon::now('Asia/Tashkent'),
                            ]
                        );

                    /*$sum = DB::table('likes')
                            ->select('likes.like as sum')
                            ->where('post_id', $request->post_id)
                            ->where('likes.like', true)->count();*/
                    $sum = DB::select('select SUM(likes.dislike) as sum, SUM(likes.like) as sum1 from likes WHERE post_id=' . $request->post_id . '');
                    return response()->json(array('msg' => $msg, 'count' => $sum[0]->sum1, 'sum' => $sum[0]->sum,), 200);
                } else {
                    $fr = DB::table('likes')->where('id', '=', $result->id)
                        ->update(
                            [
                                'like' => 0,
                                //'dislike' => 0,
                                'updated_at' => Carbon::now('Asia/Tashkent'),
                            ]
                        );
                    $sum = DB::table('likes')
                        ->select('likes.like as sum')
                        ->where('post_id', $request->post_id)
                        ->where('likes.like', 1)->count();
                    return response()->json(array('msg' => $msg, 'count' => $sum, 'ii' => $result->id), 200);
                }
            } else {
                $fr = DB::table('likes')->insert(
                    [
                        'user_id' => $request->user_id,
                        'post_id' => $request->post_id,
                        'like' => 1,
                        'dislike' => 0,
                        'created_at' => Carbon::now('Asia/Tashkent'),
                    ]
                );
                $sum = DB::table('likes')
                    ->select('likes.like as sum')
                    ->where('post_id', $request->post_id)
                    ->where('likes.like', 1)->count();
                return response()->json(array('msg' => $msg, 'count' => $sum, 'ii' => $result->post_id), 200);
            }
        } else
            return response()->json(['msg' => 'error']);
    }

    public function dislike(Request $request)
    {
        if (CRUDBooster::myId()) {
            $msg = "Success!";
            $result = DB::table('likes')
                ->where('user_id', $request->user_id)
                ->where('post_id', $request->post_id)
                ->first();
            if ($result) {
                if ($result->dislike != 1) {
                    DB::table('likes')->where('id', $result->id)
                        ->update(
                            [
                                'like' => 0,
                                'dislike' => 1,
                                'updated_at' => Carbon::now('Asia/Tashkent'),
                            ]
                        );

                    $sum = DB::select('select SUM(likes.dislike) as sum, SUM(likes.like) as sum1 from likes WHERE post_id=' . $request->post_id . '');
                    return response()->json(array('msg' => $msg, 'count' => $sum[0]->sum, 'sum' => $sum[0]->sum1,), 200);
                } else {
                    DB::table('likes')->where('id', $result->id)
                        ->update(
                            [
                                //'like' => 0,
                                'dislike' => 0,
                                'updated_at' => Carbon::now('Asia/Tashkent'),
                            ]
                        );
                    $sum = DB::select('select SUM(likes.dislike) as sum from likes WHERE post_id=' . $request->post_id . '');
                    return response()->json(array('msg' => $msg, 'count' => $sum[0]->sum), 200);
                }
            } else {
                DB::table('likes')->insert(
                    [
                        'user_id' => $request->user_id,
                        'post_id' => $request->post_id,
                        'like' => 0,
                        'dislike' => 1,
                        'created_at' => Carbon::now('Asia/Tashkent'),
                    ]
                );
                $sum = DB::select('select SUM(likes.dislike) as sum from likes WHERE post_id=' . $request->post_id . '');
                return response()->json(array('msg' => $msg, 'count' => $sum[0]->sum), 200);
            }
        } else
            return response()->json(['msg' => 'error']);
    }

}
